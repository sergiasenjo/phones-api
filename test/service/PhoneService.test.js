/**
 * Phone Service Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const sinon = require('sinon');
const PhoneService = require('../../app/service/PhoneService');
const PhoneDAO = require('../../app/dao/PhoneDAO');
const samplePhones = require('../../app/resources/samplePhones.json');

describe('Phone Service Tests', () => {

    let getPhonesStub;

    beforeEach(() => {
        getPhonesStub = sinon.stub(PhoneDAO, 'getPhones');
    });
      
    afterEach(() => {
        getPhonesStub.restore();
    });

    test('It should response the GET method', () => {
        getPhonesStub.resolves(samplePhones);
        return PhoneService.getPhones()
            .then(phones => {
                expect(phones).toBe(samplePhones);
            });
    });
})