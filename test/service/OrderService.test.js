/**
 * Order Service Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const sinon = require('sinon');
const OrderService = require('../../app/service/OrderService');
const OrderDAO = require('../../app/dao/OrderDAO');

describe('Order Service Tests', () => {

    let postOrderStub;

    beforeEach(() => {
        postOrderStub = sinon.stub(OrderDAO, 'postOrder');
    });
      
    afterEach(() => {
        postOrderStub.restore();
    });

    test('It should response the POST method', () => {
        const order = {
            'name': 'Sergio',
            'surname': 'Asenjo',
            'email': 'sergio@sergio.com',
            'phones': [
                {
                    'image': 'a.jpg',
                    'name': 'Xiaomi MI A2',
                    'description': 'Lorem ipsum dolor',
                    'price': 200
                },
            ]
        };

        const finalOrder = {
            'name': 'Sergio',
            'surname': 'Asenjo',
            'email': 'sergio@sergio.com',
            'phones': [
                {
                    'image': 'a.jpg',
                    'name': 'Xiaomi MI A2',
                    'description': 'Lorem ipsum dolor',
                    'price': 200
                },
            ],
            'total': 200
        };
        postOrderStub.resolves(finalOrder);
        return OrderService.postOrder(order)
            .then(order => {
                expect(order).toBe(finalOrder);
            });
    });
})