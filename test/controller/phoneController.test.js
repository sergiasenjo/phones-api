/**
 * Phone Controller Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const request = require('supertest');
const app = require('../../app');
const sinon = require('sinon');
const PhoneDAO = require('../../app/dao/PhoneDAO');
const samplePhones = require('../../app/resources/samplePhones.json');

describe('Phone Controller Tests', () => {

    let getPhonesStub;

    beforeEach(() => {
        getPhonesStub = sinon.stub(PhoneDAO, 'getPhones');
    });
      
    afterEach(() => {
        getPhonesStub.restore();
    });

    test('It should response the GET method with status 200', () => {
        getPhonesStub.resolves(samplePhones);
        return request(app).get("/phones").then(response => {
            expect(response.statusCode).toBe(200);
        })
    });

    test('It should response the GET method with status 500', () => {
        getPhonesStub.rejects();
        return request(app).get("/phones").then(response => {
            expect(response.statusCode).toBe(500);
        })
    });
})
