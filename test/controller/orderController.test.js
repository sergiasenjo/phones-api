/**
 * Order Controller Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const request = require('supertest');
const app = require('../../app');

describe('Order Controller Tests', () => {

    test('It should response the POST method with status 500', () => {
        return request(app).post("/order").then(response => {
            expect(response.statusCode).toBe(500);
        })
    });

})
