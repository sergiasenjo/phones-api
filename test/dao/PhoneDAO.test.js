/**
 * Phone DAO Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const sinon = require('sinon');
const PhoneDAO = require('../../app/dao/PhoneDAO');
const samplePhones = require('../../app/resources/samplePhones.json');

describe('Phone DAO Tests', () => {

    let getPhonesStub;

    beforeEach(() => {
        getPhonesStub = sinon.stub(PhoneDAO, 'getPhones');
    });
      
    afterEach(() => {
        getPhonesStub.restore();
    });

    test('It should response the GET method', () => {
        getPhonesStub.resolves(samplePhones);
        return PhoneDAO.getPhones()
            .then(phones => {
                expect(phones).toBe(samplePhones);
            });
    });
})