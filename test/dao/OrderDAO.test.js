/**
 * Phone DAO Tests
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const sinon = require('sinon');
const OrderDAO = require('../../app/dao/OrderDAO');

describe('Order DAO Tests', () => {

    let postOrderStub;

    beforeEach(() => {
        postOrderStub = sinon.stub(OrderDAO, 'postOrder');
    });
      
    afterEach(() => {
        postOrderStub.restore();
    });

    test('It should response the POST method', () => {
        postOrderStub.resolves(true);
        return OrderDAO.postOrder(true)
            .then(order => {
                expect(order).toBe(true);
            });
    });
})