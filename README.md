# Phones API

### Installation

It requires [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/) to run.

##### Clone the project

```sh
$ git clone https://bitbucket.org/sergiasenjo/phones-api.git
$ cd phones-api
```
### Usage

##### Compose with Docker

```sh
$ docker-compose up
```

##### List of endpoints:
* localhost:8080/phones
* localhost:8080/order

##### Postman
In the project, there is a folder called postman with a collection of active requests.

##### Tests
```sh
$ npm test
```

### License
##### [ISC](https://opensource.org/licenses/ISC)

### Author
**Sergio Asenjo**