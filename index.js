/**
 * Phones API
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const app = require('./app');

// Environment config
const environment = process.env.NODE_ENV || 'local';
const config = require('./config/' + environment + '.json');

// Logger config
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL || config.logLevel;

// Port
const PORT = process.env.PORT || 8082;

app.listen(PORT, function() {
  logger.info(`Phones API is listening in port ${PORT}`);
});
