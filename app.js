/**
 * App
 * @author Sergio Asenjo Alcalde
 */
'use strict';

// Express
const express = require('express');
const app = express();

// Mongo
const Mongo = require('./app/db/Mongo');
Mongo.connect();
// Inserting Sample Phones in Mongo (TODO: Remove this part of code)
const Phone = require('./app/models/phone');
Phone.remove({});
const Order = require('./app/models/order');
Order.remove({});
const samplePhones = require('./app/resources/samplePhones.json');
samplePhones.forEach((samplePhone) => {
  const phone = new Phone({
    image: samplePhone.image,
    name: samplePhone.name,
    description: samplePhone.description,
    price: samplePhone.price
  });
  phone.save();
});

// Json
app.use(express.json());

// Allow CORS
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

// Routes
require('./app/routes/routes')(app);

module.exports = app;
