/**
 * Mongo
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const mongoose = require('mongoose');
const log = require('log4js').getLogger('CommonDAO');

/**
 * Mongo
 */
class Mongo {
  /**
   * Mongo Connect
   */
  connect() {
    mongoose
        .connect(
            'mongodb://mongo:27017/phones-api',
            {useNewUrlParser: true}
        )
        .then(() => log.info('MongoDB Connected'))
        .catch((error) => log.error(error));
  }

  /**
   * Mongo Close Connection
   */
  disconnect() {
    mongoose.connection.close();
  }
}

module.exports = new Mongo();
