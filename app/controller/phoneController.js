/**
 * Phone Controller
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const PhoneService = require('../service/PhoneService');
const log = require('log4js').getLogger('phoneController');

/**
 * Get phones
 * @param req request
 * @param res response
 */
function getPhones(req, res) {
  log.info(`::getPhones | GET `);

  PhoneService.getPhones()
      .then((phones) => {
        res.send(phones);
      })
      .catch((error) => {
        res.status(500).send(error);
      });
}

module.exports = {
  getPhones
};
