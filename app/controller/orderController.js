/**
 * Order Controller
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const OrderService = require('../service/OrderService');
const log = require('log4js').getLogger('phoneController');

/**
 * Post orders
 * @param req request
 * @param res response
 */
function postOrder(req, res) {
  log.info(`::postOrder | POST `);

  const order = req.body;

  OrderService.postOrder(order)
      .then((order) => {
        log.info(`Name: ${order.name}`);
        log.info(`Surname: ${order.surname}`);
        log.info(`Email: ${order.email}`);
        log.info('Phones: ' + JSON.stringify(order.phones));
        log.info(`Total price: ${order.total}`);
        res.send(order);
      })
      .catch((error) => {
        res.status(500).send(error);
      });
}

module.exports = {
  postOrder
};
