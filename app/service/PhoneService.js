/**
 * Phone Service
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const PhoneDAO = require('../dao/PhoneDAO');

/**
 * Phone service
 */
class PhoneService {
  /**
   * Get phones
   */
  getPhones() {
    return PhoneDAO.getPhones();
  }
}

module.exports = new PhoneService();
