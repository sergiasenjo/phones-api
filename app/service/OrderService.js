/**
 * Order Service
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const OrderDAO = require('../dao/OrderDAO');

/**
 * Order service
 */
class OrderService {
  /**
   * Post order
   */
  postOrder(order) {
    order.total = order.phones
        .map(({price}) => price)
        .reduce((beforePrice, actualPrice) => beforePrice + actualPrice);
    return OrderDAO.postOrder(order);
  }
}

module.exports = new OrderService();
