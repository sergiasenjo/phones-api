/**
 * Routing
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const phoneController = require('../controller/phoneController');
const orderController = require('../controller/orderController');

module.exports = function(app) {
  // Get all phones
  app.get('/phones', phoneController.getPhones);

  // Post an order
  app.post('/order', orderController.postOrder);
};
