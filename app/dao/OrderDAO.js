/**
 * Order DAO
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const Order = require('../models/order');

/**
 * Order DAO
 */
class OrderDAO {
  /**
   * Post order
   */
  postOrder(order) {
    const finalOrder = new Order({
      name: order.name,
      surname: order.surname,
      email: order.email,
      phones: order.phones,
      total: order.total
    });
    return finalOrder.save();
  }
}

module.exports = new OrderDAO();
