/**
 * Phone DAO
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const Phone = require('../models/phone');

/**
 * Phone DAO
 */
class PhoneDAO {
  /**
   * Get phones
   */
  getPhones() {
    return Phone.find();
  }
}

module.exports = new PhoneDAO();
