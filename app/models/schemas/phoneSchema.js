/**
 * Phone Schema
 * @author Sergio Asenjo Alcalde
 */
'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhoneSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  }
});

module.exports = PhoneSchema;
