/**
 * Order Schema
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  phones: {
    type: Array,
    required: true
  },
  total: {
    type: Number,
    required: true
  }
});

module.exports = OrderSchema;
