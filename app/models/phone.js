/**
 * Phone Model
 * @author Sergio Asenjo Alcalde
 */
'use strict';

const mongoose = require('mongoose');

const PhoneSchema = require('./schemas/phoneSchema');

const Phone = mongoose.model('Phone', PhoneSchema);

module.exports = Phone;
